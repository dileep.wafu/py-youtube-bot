
import csv
import sys
import os
print("Step 2 : Generate Videos, Mix Audio (x.mp3) and Image (x.jpg)")
start = int(input("Enter start range: "))
end = int(input("Enter end range: "))
filesToRead = []
for i in range(start,end+1):
    filesToRead.append(i)
print("Videos I will create .wmv type: ")
print(filesToRead)
print('\n\n')

for justFileName in filesToRead:
    cmd = f"python ./util/single_video_generate.py {justFileName} {justFileName} {justFileName}"
    print("Executing:")
    print(cmd)
    os.system(cmd)
