import glob
import ntpath
from simple_youtube_api.Channel import Channel
from simple_youtube_api.LocalVideo import LocalVideo

# loggin into the channel
channel = Channel()
channel.login("./secret/client_secret.json", "./secret/credentials.storage")


# helpers-----------------------------------------------------------------
def getFileNameOfPath(path):
    head, tail = ntpath.split(path)
    return tail or ntpath.basename(head)

def uploadVideo(fileNameWithExt, description, discourseTitle, totalParts, tags):
    justFileName = fileNameWithExt.split('.')[0]
    title = f"{discourseTitle} || Part {justFileName}/{totalParts} || OSHO English Discourse || Medicess"
    print("🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺")
    print("🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺")
    print("🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺")
    print(f"\n\nStarted uploading :: {fileNameWithExt}")
    print(title)
    print("🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺")
    print("🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺")
    print("🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺")


    # setting up the video that is going to be uploaded
    video = LocalVideo(file_path=f"./storage/upload/{fileNameWithExt}")

    # setting snippet
    video.set_title(title)
    video.set_description(description)
    video.set_tags(tags)
    video.set_category(28)  # Science & Technology
    video.set_default_language("en-US")

    # setting status and playlist and other
    video.set_embeddable(True)
    video.set_license("youtube")
    video.set_privacy_status("public")
    video.set_public_stats_viewable(True)
    video.set_made_for_kids(True)

    # setting thumbnail
    video.set_thumbnail_path(f'./storage/images/{justFileName}.jpg')
    print("Started uploading...")

    # uploading video and printing the results
    video = channel.upload_video(video)
    print("😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍")
    print("😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍")
    print("😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍")
    print(f"Uploaded Video successfully")
    print(f'{title}\n\n')
    print("😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍")
    print("😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍")
    print("😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍")
# ------------------------------------------------------------------------


#1. get all .wmv files
outputFiles = glob.glob('./storage/upload/*.wmv')
outputFileNamesWithExt = []

#2. get fileNames
print("✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨")
print("✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨")
print("Files that will be uploaded ::")
for filePath in outputFiles:
    fileName = getFileNameOfPath(filePath)
    outputFileNamesWithExt.append(fileName)
    print(fileName)
print("✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨")
print("✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨")
print("✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨")

# 3. Get video info
print("\n\nI will ask you some details that make title like this:")
print("<<Discourse Title>> || Part <<filename>>/<<Total parts>> || OSHO English Discourse || Medicess")
discourseTitle = input("\nEnter Discourse Title : \n")
totalParts = input("Total parts of discourse: \n")
description = input("Enter description: \n")
defaultTags = ['Medicess', 'medicess', 'medi cess', 'medi', 'cess', 'medicess osho', 'medicess osho speech', 'osho speech', 'osho english speech', 'osho english discourse',
               'osho discourses', 'osho pravachan', 'osho medicess', 'osho talks', 'osho', 'osho hindi', 'osho hindi pravachan', 'osho hindi discourse', 'osho talk', 'osho light']
tags = input("\n\nEnter tags seperated with comma :\n").split('.')
tags = tags+defaultTags
print(tags)

# 4. iterate over all fileNames and upload them.
for (index, fileNameWithExt) in enumerate(outputFileNamesWithExt):
    uploadVideo(fileNameWithExt, description,
                discourseTitle, totalParts, tags)
