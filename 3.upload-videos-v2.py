import datetime
from googleapiclient.http import MediaFileUpload
import glob
import ntpath
import pickle
import os
from google_auth_oauthlib.flow import Flow, InstalledAppFlow
from googleapiclient.discovery import build
from googleapiclient.http import MediaFileUpload, MediaIoBaseDownload
from google.auth.transport.requests import Request

# Config ----------------------------------------------------------------------------------------
API_NAME = 'youtube'
API_VERSION = 'v3'
SCOPES = ['https://www.googleapis.com/auth/youtube.upload']
baseFilesLocation = './storage/upload/'
baseThumbnailsLocation = './storage/images/'
pickleFileLocation = './secret/token.pickle'
CLIENT_SECRET_FILE = './secret/client_secret.json'


# Init ------------------------------------------------------------------------------------------
# 1. Authentiacates if token not exist.


def createService(client_secret_file, api_name, api_version, *scopes):
    print(client_secret_file, api_name, api_version, scopes, sep='-')
    CLIENT_SECRET_FILE = client_secret_file
    API_SERVICE_NAME = api_name
    API_VERSION = api_version
    SCOPES = [scope for scope in scopes[0]]
    print(SCOPES)

    cred = None

    # print(pickle_file)

    if os.path.exists(pickleFileLocation):
        with open(pickleFileLocation, 'rb') as token:
            cred = pickle.load(token)

    if not cred or not cred.valid:
        if cred and cred.expired and cred.refresh_token:
            cred.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                CLIENT_SECRET_FILE, SCOPES)
            cred = flow.run_local_server()

        with open(pickleFileLocation, 'wb') as token:
            pickle.dump(cred, token)

    try:
        service = build(API_SERVICE_NAME, API_VERSION, credentials=cred)
        print(API_SERVICE_NAME, 'service created successfully !')
        # service.
        return service
    except Exception as e:
        print('Unable to connect.')
        print(e)
        return None


service = createService(
    CLIENT_SECRET_FILE, API_NAME, API_VERSION, SCOPES)


# HELPERS --------------------------------------------------------------------------------------
def getFileNameOfPath(path):
    head, tail = ntpath.split(path)
    return tail or ntpath.basename(head)


def generateBody(title, description, tags):
    requestBody = {
        'snippet': {
            'categoryId': 28,
            'title': title,
            'description': description,
            'tags': tags,
            'defaultLanguage': 'en-US'
        },
        'status': {
            'privacyStatus': 'public',
            # 'publishAt': upload_date_time,
            'selfDeclaredMadeForKids': True,
            'embeddable': True,
            'license': 'youtube',
        },
        'notifySubscribers': False
    }
    return requestBody


def uploadVideo(requestBody, filePath, thumbnailPath):
    mediaFile = MediaFileUpload(filePath)

    # Upload Video
    response_upload = service.videos().insert(
        part='snippet,status',
        body=requestBody,
        media_body=mediaFile
    ).execute()

    # Upload Thumbnail (size should be lessthan 2mb)
    service.thumbnails().set(
        videoId=response_upload.get('id'),
        media_body=MediaFileUpload(thumbnailPath)
    ).execute()

# Main -----------------------------------------------------------------------------------------


def main():
    # 1. get all .wmv files
    outputFiles = glob.glob(f'{baseFilesLocation}*.wmv')
    outputFileNamesWithExt = []

    # 2. get fileNames
    print("✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨")
    print("✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨")
    print("Files that will be uploaded ::")
    for filePath in outputFiles:
        fileName = getFileNameOfPath(filePath)
        outputFileNamesWithExt.append(fileName)
        print(fileName)
    print("✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨")
    print("✨✨✨✨✨✨✨✨✨✨✨✨✨✨✨")

    # 3. Get video info
    print("\n\nI will ask you some details that make title like this:")
    print("<<Discourse Title>> || Part <<filename>>/<<Total parts>> || OSHO English Discourse || Medicess")
    discourseTitle = input("\nEnter Discourse Title : \n")
    totalParts = input("Total parts of discourse: \n")
    description = input("Enter description: \n")
    defaultTags = ['Medicess', 'medicess', 'medi cess', 'medi', 'cess', 'medicess osho', 'medicess osho speech', 'osho speech', 'osho english speech', 'osho english discourse',
                   'osho discourses', 'osho pravachan', 'osho medicess', 'osho talks', 'osho', 'osho hindi', 'osho hindi pravachan', 'osho hindi discourse', 'osho talk', 'osho light']
    tags = input("\n\nEnter tags seperated with comma :\n").split('.')
    tags = tags+defaultTags
    print(tags)

    # 4. iterate over all fileNames and upload them.
    for fileNameWithExt in outputFileNamesWithExt:
        justFileName = fileNameWithExt.split('.')[0]
        title = f"{discourseTitle} || Part {justFileName}/{totalParts} || OSHO English Discourse || Medicess"
        requestBody = generateBody(title, description, tags)
        filePath = baseFilesLocation+fileNameWithExt
        thumbnailPath = baseThumbnailsLocation+justFileName+'.jpg'
        print("🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺")
        print("🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺")
        print(f"Uploading :: {filePath}")
        uploadVideo(requestBody, filePath, thumbnailPath)
        print("😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍")
        print(f"Uploaded Video successfully")
        print("😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍")

main()
