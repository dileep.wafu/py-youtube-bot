import os
import sys

imageName = sys.argv[1]
audioName = sys.argv[2]
outputName = sys.argv[3]

print("📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️")
print("📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️")
print("📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️")
print(f"Generating Video:: {outputName} .... processing....")
print("📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️")
print("📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️")
print("📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️📽️")


cmd = f"ffmpeg -loop 1 -framerate 2 -i ./storage/images/{imageName}.jpg -i ./storage/audio/{audioName}.mp3 -c:v libx264 -preset medium -tune stillimage -crf 18 -c:a copy -shortest -pix_fmt yuv420p ./storage/output/{outputName}.wmv"
os.system(cmd)

print("🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥")
print("🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥")
print("🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥")
print(f"\n\nDone ! {outputName}.wmv")
print("🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥")
print("🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥")
print("🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥")

